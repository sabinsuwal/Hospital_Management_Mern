var express= require('express')
var path= require('path')
let app=express()

app.use(express.static(path.join(process.cwd(),'build')))
app.use('/',function(req,res,next){
    res.sendFile(path.join(process.cwd(),'build/index.html'))
})

app.listen(process.env.PORT || 8080, function(err){
 if (!err){
     console.log('listening 8080')
 }
});