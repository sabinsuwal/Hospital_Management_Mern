import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import {store} from './store'

import { BrowserRouter, Navigate, Route, Routes, useNavigate } from 'react-router-dom';
import { HomeLayout } from './component/home/home_layout'
import {Home,Login, Register,Doctor,RegisterAppointment} from './component/pages'
import { DoctorHome,ViewAllAppointment,ViewSelfAppointment,DoctorNavbar } from './component/doctor';
import { AdminHomeLayout } from './component/admin/homelayout.admin';
import { AdminHome } from './component/admin/home.admin';
import { RegisterDoctor } from './component/admin/doctor/register.doctor';
import DoctorList from './component/admin/doctor/view.doctor.admin';
import { EditDoctor } from './component/admin/doctor/edit.doctor.admin';
import { ViewAppointment } from './component/admin/appointment/appointment.view';
import {EditAppointment }from './component/admin/appointment/edit.appointment';
import { DoctorHomeLayout } from './component/doctor/homelayout.doctor';




function ErrorPage(){
  return (
    <>
    404 not found
    </>
  )
}


function Logout(){
  localStorage.clear()
  let navigate= useNavigate()
  useEffect(()=>{
    navigate('/login')
  })
  return (
    <>
    </>
);
}
function PrivateRoute({component: Component}){
  let is_logged_in = Boolean(localStorage.getItem('_token'));
  
  return is_logged_in === true ? Component : <Navigate to='/login'></Navigate>
}

function UserRoute({component:Component}){
  let is_logged_in= Boolean(localStorage.getItem('_token'))
  if (is_logged_in){
    let user= JSON.parse(localStorage.getItem('_user'))
    if (user.role=='user'){
      return Component
    }
    else{
      return (<Navigate to ='/'></Navigate>)
    }
  }else{
    return <Navigate to='/login'></Navigate>;
  }
}



function AdminRoute({component:Component}){
  let is_logged_in= Boolean(localStorage.getItem('_token'))
  if (is_logged_in){
    let user= JSON.parse(localStorage.getItem('_user'))
    if (user.role=='admin'){
      return Component
    }
    else{
      return (<Navigate to ='/'></Navigate>)
    }
  }else{
    return <Navigate to='/login'></Navigate>;
  }
}

function DoctorRoute({component:Component}){
  let is_logged_in= Boolean(localStorage.getItem('_token'))
  if (is_logged_in){
    let user= JSON.parse(localStorage.getItem('_user'))
    if (user.role=='doctor'){
      return Component
    }
    else{
      return (<Navigate to ='/'></Navigate>)
    }
  }else{
    return <Navigate to='/login'></Navigate>;
  }
}




export function App() {

    return (
      // <h3>hello </h3>
   
        <Provider store={store}>
        <BrowserRouter>
        <Routes>
        <Route path='/' element={<HomeLayout />}>
          <Route index element={<Home></Home>}></Route>
          <Route path='/login' element={<Login></Login>}></Route>
          <Route path='/register' element={<Register></Register>}></Route>
        
          <Route path='/doctor' element={< UserRoute component={<Doctor />}></UserRoute>}></Route>
           <Route path='/appointment' element={<UserRoute component= {<RegisterAppointment />}></UserRoute>}></Route>
          
        </Route>
        <Route path='/doctors' element={<DoctorRoute component={<DoctorHomeLayout />}></DoctorRoute>}>
          <Route index element={<DoctorHome></DoctorHome>}></Route>
          <Route path='self' element={<ViewSelfAppointment></ViewSelfAppointment>}></Route>
          <Route path='view' element={< ViewAllAppointment />}></Route>
          
        </Route>



        <Route path='/admin' element={<AdminRoute component={<AdminHomeLayout />}></AdminRoute>}>
          <Route index element={<AdminHome></AdminHome>}></Route>
          <Route path='doctor/register' element={<RegisterDoctor></RegisterDoctor>}></Route>
          <Route path='doctor' element={< DoctorList />}></Route>
          <Route path='doctor/:id' element={< EditDoctor />}></Route>
          <Route path='appointment' element={< ViewAppointment />}></Route>
          <Route path='appointment/appoint/:id' element={< EditAppointment />}></Route>
 



        </Route>
        <Route path='/logout' element={<PrivateRoute component={<Logout />}> </PrivateRoute>}></Route>
         <Route path='/*' element={<ErrorPage></ErrorPage>}></Route>
        </Routes>
        </BrowserRouter>
        </Provider>
       
   
     
    );
  
}


