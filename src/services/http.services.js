import axios from 'axios';

const getHeaders=()=>{
    const headers={
        'content-type': "application/json"
    }
    return headers
}

const axiosReq= axios.create({
    baseURL: process.env.REACT_APP_BASE_API_URL,
        responseType:'json',
        timeout:30000,
        timeoutErrorMessage:'Request timed out',

});

const postItem=(url,data, is_strict=false)=>{
    let config=getHeaders()
    if (is_strict){
        config['authorization']=localStorage.getItem('_token')
    }
    return axiosReq.post(url,data,{
        headers:config
    })
}
const getItem=(url,is_strict=false)=>{
    let config=getHeaders()
    if(is_strict){
        config['authorization']=localStorage.getItem('_token')
    }
    return axiosReq.get(url,{
        headers:config
    })
}
const updateItem=(url,data,is_strict=false)=>{
    let config= getHeaders();
    if (is_strict){
        config['authorization']=localStorage.getItem('_token')

    }return axiosReq.put(url,data,{
        header:config
    })
}

const deleteItem=(url,is_strict=false)=>{
    let config=getHeaders()
    if (is_strict){
        config['authorization']=localStorage.getItem('_token')
    }
    return axiosReq.delete(url,{
        header:config
    })
}



export const http={
    postItem,
    getItem,
    updateItem,
    deleteItem
}