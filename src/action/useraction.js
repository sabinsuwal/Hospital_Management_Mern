
import { toast } from "react-toastify";
import { http } from "../services/http.services";

export const UserActionTypes = {
  LOGIN: "lOGIN",
};

export const login = (data) => (dispatch) => {
  http
    .postItem("/login", data)
    .then((res) => {
      if (res.data.status) {
        let user = {
          id: res.data.data.user._id,
          firstname: res.data.data.user.firstname,
          email: res.data.data.user.email,
          role: res.data.data.user.Role,
        };
        
        
        localStorage.setItem("_token", res.data.data.token);

        localStorage.setItem("_user", JSON.stringify(user));
     
        return dispatch({
          type: UserActionTypes.LOGIN,
          payload: res.data.data.user,
        });
      } else {
        toast.error('invalid  username or password')
     
        return dispatch({
          type: UserActionTypes.LOGIN,
          payload: {},
        });
      }
    })
    .catch((err) => {
      console.log("errors", err);
    });
};
