import { Home } from "./home/home";
import { Login } from './login/Login'
import {Register} from './register/Register'
import {RegisterAppointment } from './appointment/register.appointment'
import { Doctor } from './doctor/doctor'


export {
    Home,
    Login,
    Register,
    RegisterAppointment,

    Doctor,

}