import React, { useEffect, useState } from "react";
import DataTable from "react-data-table-component";

import { http } from "../../../services/http.services";
import { ActionBtns } from "../../common/action-btns.component";
import { toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'



export function RegisterAppointment() {
  let [problem, setProblem] = useState("");
  let [age, setAge] = useState(0);
  let [user, setUser] = useState("");
  let [appointment_id, setAppointment_id]=useState('')
 
  let [appointment, setAppointment] = useState();

  let [isLoading1, setIsLoading1] = useState(false);

  const columns = [
    { name: "Name", selector: (row) => row?.user_id?.firstname },
    { name: "Gender", selector: (row) => row?.user_id?.gender },
    { name: "Problem", selector: (row) => row?.problem },
    { name: "Doctor", selector: (row) => row?.doctorname },

    {
      name: "Appointment-Date",
      selector: (row) => row?.appointmentDate,
    },
    { name: "Appointment-Time", selector: (row) => row?.appointmentTime },
    {
      name: "Actions",
      selector: (row) => (
        <ActionBtns
          onEdit={editAppointment}
          reg={true}
          id={row._id}
          onDelete={deleteAppointment}
        ></ActionBtns>
      ),
    },
  ];

  const editAppointment = (id) => {
   
    http
      .getItem("/admin/appointment/appoint/" + id)
      .then((res) => {
        if (res.data.status) {
          toast.success("Edit appointment")
          setAppointment_id(res.data.data._id)
          setProblem(res.data.data.problem);
          setAge(res.data.data.age);
   
         
         
        } else {
          toast.error('Error')
          
          console.log(res.data.data.msg);
        }
      })
      .catch((er) => {
        toast.error('Error')
        console.log(er);
      });
  };

  const deleteAppointment = (id) => {
    http
      .deleteItem("/admin/appointment/" + id)
      .then((res) => {
       
        if (res.data.status) {
          toast.success('sucessfuly deleted')
          if (isLoading1) {
            setIsLoading1(false);
          } else {
            setIsLoading1(true);
          }
        } else {
          toast.error('Error')
          console.log("err in deletering");
        }
      })
      .catch((err) => {
        toast.error('Error')
        console.log("err", err);
      });
  };

  useEffect(() => {
    let user = JSON.parse(localStorage.getItem("_user")) || {};
    setUser(user);
    toast.success('welcome to registration page')
    if (isLoading1) {
      setIsLoading1(false);
    } else {
      setIsLoading1(true);
    }
  }, []);

  useEffect(() => {
    http
      .getItem("/admin/appointment/" + user.id)

      .then((res) => {
        if (res.data.status) {
        
          let all_result = [];
          all_result = res.data.data.map((o) => o);
          setAppointment(all_result);
        }
      })
      .catch((err) => {
        console.log("err in retirv", err);
      });
  }, [isLoading1]);

  const onSubmit = (e) => {
    e.preventDefault();
    let data = {
      user_id: user.id,
      problem: problem,
      age: age,
    };
    setProblem('')
    setAge(0)
  if (appointment_id!=0){
    http
    .updateItem("/admin/appointment/"+appointment_id, data)
    .then((res) => {
      if (res.data.status) {
        toast.success('update successfuly')
        if (isLoading1) {
          setIsLoading1(false);
        } else {
          setIsLoading1(true);
        }
      } else {
        console.log(res.data.msg);
      }
    })
    .catch((err) => {
      console.log(err);
    });

  }else{
    http
      .postItem("/admin/appointment", data)
      .then((res) => {
        if (res.data.status) {
          toast.success('submit successfuly')
          if (isLoading1) {
            setIsLoading1(false);
          } else {
            setIsLoading1(true);
          }
        } else {
          console.log(res.data.msg);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    }
  };
  return (
    <>
      <div className="register_container">
      <h3 className="mb-2 text-center ">Register Appointment</h3>
        <div className="wrapper_container">

        
      <form
       
        onSubmit={onSubmit}
      >
        <div className="row">
          <div className="container">
            <div className="mb-2">
              <label>username</label>
              <input
                type="textbox"
                name="problem"
                defaultValue={user.firstname}
                className="form-control"
                disabled
                required
              ></input>
            </div>
            <div className="mb-2">
              <label>problems</label>
              <input
                type="textbox"
                value={problem}
                name="problem"
                onChange={(e) => setProblem(e.target.value)}
                className="form-control"
                placeholder="problem......"
                required
              ></input>
            </div>
            <div className="mb-2">
              <label>AGE</label>
              <input
                type="number"
                value={age}
                min={1}
                max={100}
                name="age"
                onChange={(e) => setAge(e.target.value)}
                className="form-control"
                placeholder="Age"
                required
              ></input>
            </div>
      

            <div className="d-grid mb-2">
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </div>
        </div>
      </form>
      </div>

      </div>

      <h4 className="appointment"
       style={{margin:'30px ', textAlign:'center', fontWeight:'bold'}}
      >Appointment List</h4>

      <hr />
      <DataTable columns={columns} data={appointment} pagination responsive />
    </>
  );
}
