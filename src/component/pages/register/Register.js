import React, { useState } from "react";

import { NavLink, useNavigate } from "react-router-dom";
import { http } from "../../../services/http.services";
import { toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'

import './register.css'

export function Register() {
  let [firstname, setFirstName] = useState("");
  let [lastname, setLastName] = useState("");
  let [gender, setGender] = useState("male");
  let [address, setAddress] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let navigate= useNavigate()
  const onSubmit = (e) => {
    e.preventDefault();
    let data_reg = {
      firstname: firstname,
      lastname: lastname,
      gender: gender,
      address: address,
      email: email,
      password: password,
    };

    http.postItem('/user',data_reg)
    .then((res)=>{
     
      if (res.status==200){
          toast.success('welcome, to login page')
          navigate('/login')
      }
    })
    .catch((err)=>{
      toast.error('Error, in registering')
      console.log(err)
    })
  };

  return (
    <>
        <div className="register_container">
      <h3>Register</h3>
          <div className="register_wrapper">
      <form
        className="form"
        onSubmit={onSubmit}
      >
            <div className="form_control">
              <label>First Name</label>
              <input
                type="text"
                name="firstname"
                onChange={(e) => setFirstName(e.target.value)}
                className="form-control"
                placeholder="firstName"
                required
              ></input>
            </div>
            <div className="form_control">
              <label>Last Name</label>
              <input
                type="text"
                name="lastname"
                onChange={(e) => setLastName(e.target.value)}
                className="form-control"
                placeholder="lastName"
                required
              ></input>
            </div>

            <div className="form_control">
              <label>Select Gender</label>
              <select
                
               
                onChange={(e) => setGender(e.target.value)}
              ><option disabled>Select </option>
                <option value="male">Male</option>
                <option value="female">Female</option>
                <option value="other">Other</option>
              </select>
            </div>
            <div className="form_control">
              <label>Address</label>
              <input
                type="text"
                className="form-control"
                placeholder="address"
                required
                onChange={(e) => setAddress(e.target.value)}
              ></input>
            </div>

            <div className="form_control">
              <label>Email</label>
              <input
                type={"email"}
                className="form-control"
                placeholder="abc@gmail.com"
                required
                onChange={(e) => setEmail(e.target.value)}
              ></input>
            </div>
            <div className="form_control">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                placeholder="password"
                minLength={8}
                maxLength={15}
                required
                onChange={(e) => setPassword(e.target.value)}
              ></input>
            </div>
            <div className="form_control ">
              <button type="submit" >
                Sign Up
              </button>
            </div>
            <div className="form_control">

            <p>
              Already registered ? <NavLink  className ='NavLink' to="/login">Log in</NavLink>
            </p>
            </div>
      </form>
          </div>
        </div>
    </>
  );
}
