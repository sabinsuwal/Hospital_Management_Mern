import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { connect } from 'react-redux'
import {login} from '../../../action/useraction'
import { toast,ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'

import './login.css'



function LoginComponent(props) {
  let [email, setEmail] = useState("");
  let [isdoctor, setIsDoctor]=useState(false)
  let [password, setPassword] = useState("");
  let navigate = useNavigate();
 

  useEffect(()=>{
    let is_logged= Boolean(localStorage.getItem('_token'))
    if (is_logged==true){
     
      let user=JSON.parse(localStorage.getItem('_user'))
     
      if (user.role=='user'){
        toast.success('welome,  appointment page')
        navigate('/appointment')
      }
      if (user.role=='doctor'){
        toast.success('welome,  doctor page')
        navigate('/doctors')
      }
      if (user.role=='admin'){
        toast.success('welome,  admin page')
        navigate('/admin')
      }
     
    }
  },[props])

  const handleSubmit = (ev) => {
   
    ev.preventDefault();
    let data = {
      email: email,
      password: password,
      isdoctor:isdoctor
    };
   props.loginFunction(data) 
  };


  return (
    <>
     <div className="register_container">
     <h3 >Login</h3>
     <div className="register_wrapper">

     
      <form
        onSubmit={handleSubmit}
        className='form'
      >
  
          <div className="form_control">
            <label>Email</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              name="email"
              required
              defaultValue={email || ""}
              onChange={(ev) => {
                setEmail(ev.target.value);
              }}
            />
          </div>
          <div className="form_control">
            <label>Password</label>
            <input
              type="password"
              name="password"
              className="form-control"
              placeholder=" password"
              required
              onChange={(ev) => {
                setPassword(ev.target.value);
              }}
            />
          </div>

          <div className="form_control checkbox">
            <label>Doctor</label>
            <input
              type='checkbox'
              name="password"
               className="check_doctor"
              checked={isdoctor}
              onChange={() => {
                setIsDoctor(!isdoctor);
              }}
            />
          </div>
          <div className="form_control">
            <button type="submit">
              Submit
            </button>
          </div>
          <div className="form_control">

          <p >
            No account ? <NavLink 
            className ='NavLink'
            to="/register">Sign in</NavLink>
          </p>
          </div>
       
      </form>

      </div>

     </div>
    </>
  );
}

const mapStateToProps= (rootStore)=>({
user:rootStore.user.user

})

const mapDispatchToProps={
  loginFunction:login
}

export const Login=connect(mapStateToProps,mapDispatchToProps)(LoginComponent)