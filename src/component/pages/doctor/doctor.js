import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import { http } from '../../../services/http.services'
import { toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'
export function Doctor() {
  let [doctor,setDoctor]= useState()
const columns=[
    {name:'Name',
    selector:row=>row?.firstname
},
{name:'Gender',
selector:row=>row?.gender
},
{name:'Address',
selector:row=>row?.address
},
{name:'Department',
selector:row=>row?.department
},
]

useEffect(()=>{
  http.getItem('/admin/doctor')
  .then((re)=>{
    if (re.data.status){
      toast.success('View, Doctor List')
      let all_result=[]
      all_result=re.data.data.map((o)=>o)
      setDoctor(all_result)
  }

  })
  .catch((err)=>{
    toast.error('Problem , in showing doctor list')
    
  })
},[])


  return (
    <>
    <h3 style={{fontWeight:'bold', textAlign:'center', margin:'30px '}}>
      Doctor list</h3>
      <hr/>
      <DataTable
            columns={columns}
            data={doctor}
            pagination
            responsive
        />

      </>
  )
}
