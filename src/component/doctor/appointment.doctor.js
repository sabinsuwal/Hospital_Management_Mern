import React, { useEffect, useState } from "react";
import DataTable from "react-data-table-component";

import { http } from "../../services/http.services";


export function ViewSelfAppointment() {
    let [doctor, setDoctor]= useState({})
    let [loading, setLoading]= useState(false)
    
  const columns = [
    { name: "Name", selector: (row) => row?.user_id?.firstname },
    { name: "Gender", selector: (row) => row?.user_id?.gender },
    { name: "Problem", selector: (row) => row?.problem },
    { name: "Doctor", selector: (row) => row?.doctorname },

    {
      name: "Appointment-Date",
      selector: (row) => row?.appointmentDate,
    },
    { name: "Appointment-Time", selector: (row) => row?.appointmentTime },
  ];

  let [appointment, setAppointment] = useState();

  useEffect(()=>{
      let doct= JSON.parse(localStorage.getItem('_user'))
     
      setDoctor(doct)
      if (loading){
        setLoading(false)
      }else{
        setLoading(true)
      }
   
  },[])

  useEffect(() => {
    console.log(doctor)
    http
      .getItem("/admin/appointment/doctor/"+doctor.firstname)

      .then((res) => {
        if (res.data.status) {
          console.log(res)
          let all_result = [];

          all_result = res.data.data.map((o) => o);
          setAppointment(all_result);
        }
      })
      .catch((err) => {
        console.log("err in retirv", err);
      });
  }, [doctor]);

  return (
    <>
       <h4 style={{ textAlign:'center', margin:'40px 0px', 
      fontWeight:'bold',
      fontSize:'30px'}}>
      Appointment List</h4>

      <hr />
      <DataTable columns={columns} data={appointment} pagination responsive />
    </>
  );
}
