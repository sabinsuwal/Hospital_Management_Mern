import { DoctorHome } from "./home.doctor";
import { ViewSelfAppointment } from "./appointment.doctor";
import { ViewAllAppointment } from "./view.all.appointment";
import { DoctorNavbar} from "./topnavbar.doctor";


export {
    DoctorHome,
    ViewSelfAppointment,
    ViewAllAppointment,
    DoctorNavbar
}