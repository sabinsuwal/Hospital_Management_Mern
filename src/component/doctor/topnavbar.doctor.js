import React, { useEffect, useState } from "react";

import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
function DoctorNavbarComponent(props) {
    let [user,setUser]= useState('')
    useEffect(() => {
      let user1 = JSON.parse(localStorage.getItem("_user")) || {};
      setUser(user1)
    }, [props]);
 

  return (
    <>

<div className="nav_container">
          <nav className="main_navbar">
            <div className="navbar_img">
            <img
             src={process.env.PUBLIC_URL + "img/hospital.jpeg"}
              width="60px"
              height="30px"
           
              alt="React"
            />
            </div>
            <div className="navbar_list">
          
              <div className="navbar_left">
                <ul>
                  <li>
                    <NavLink to='/doctors'
                    className='NavLink'
                    >
                  Home
                </NavLink>
                </li>
                <li>

                <NavLink className="NavLink" 
                to="/doctors/self">
                  Appointment
                </NavLink>
              
                </li>
                  <li>

                <NavLink to="/doctors/view"
                className='NavLink'
                >
                  Patient
                </NavLink>
                  </li>
                    </ul>

                <div className="nav_login">
                 
              
                <NavLink to="/logout"
                style={{marginTop:'-8px'}}
                className='NavLink'
                >Logout</NavLink>
                </div>
               
                 
                </div>


           
            </div>

          </nav>

         </div>



    </>
  );
}

const mapStateToProps= (rootStore)=>({
  user:rootStore.user.user

})


const mapDispatchToProps = {};
export const DoctorNavbar = connect(
  mapStateToProps,
  mapDispatchToProps
)(DoctorNavbarComponent);
