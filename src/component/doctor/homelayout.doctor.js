import React, { Component } from 'react'
import { Outlet } from 'react-router-dom'
import { Footer } from '../footer/footer';
import { ToastContainer } from 'react-toastify';
import { DoctorNavbar } from './topnavbar.doctor';

export class DoctorHomeLayout extends Component {
  render() {
    return (
     <>
      <ToastContainer></ToastContainer>
       <DoctorNavbar />
        <Outlet>
          
          </Outlet>
          <Footer />
     </>
     
   
    )
  }
}
