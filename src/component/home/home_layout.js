import React, { Component } from 'react'
import { Outlet } from 'react-router-dom'
import { Footer } from '../footer/footer'
import { ToastContainer } from 'react-toastify';
import { TopNavbar } from '../header/main-navbar-component'
export class HomeLayout extends Component {
  render() {
    return (
     <>
  
      
       <TopNavbar />
        <Outlet>
          
          </Outlet>
          <Footer />
     </>
     
   
    )
  }
}
