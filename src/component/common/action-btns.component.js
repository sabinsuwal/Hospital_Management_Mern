import { FaPen, FaTrash } from 'react-icons/fa'
import { NavLink } from 'react-router-dom'

import Swal from 'sweetalert2'
export function ActionBtns(props){
    const confirmDelete=()=>{
        Swal.fire({
            title:'Are you sure?',
            text:"You won't be able to revert this  ",
            icon:'warning',
            showCancelButton:true,
            confirmButtonColor:'#3085d6',
            cancelButtonColor:'#d33',
            confirmButtonText:'yes, delete it !'
        }).then((result)=>{
            if (result.isConfirmed){
                props.onDelete(props.id)
            }
        })
    }

    const edit=()=>{
        props.onEdit(props.id)
    }
    
    return (
        <>
        {props.reg ?(
            <button onClick ={edit} className='btn btn-sm btn-success btn-circle'>
            <FaPen></FaPen>
            </button>
        ):(
            <NavLink to ={props.editUrl+'/'+props.id} className='btn btn-sm btn-success btn-circle'>
            <FaPen></FaPen>
            </NavLink> 
        )}
        
         <button onClick={confirmDelete} className='btn btn-sm btn-danger btn-circl'>
             <FaTrash />
             </button>   
        </>
    )
}