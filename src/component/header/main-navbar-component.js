import React, { useEffect, useState } from "react";
import './main_navbar.css'

import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
function TopNavbarComponent(props) {
 
  let [user,setUser]= useState('')
  useEffect(() => {
    let user1 = JSON.parse(localStorage.getItem("_user")) || {};
    if (user1.role=='user'){

      setUser(user1)
    }
  }, [props]);

  return (
    <>
         <div className="nav_container">
          <nav className="main_navbar">
            <div className="navbar_img">
            <img
              src={process.env.PUBLIC_URL + "img/hospital.jpeg"}
              width="60px"
              height="30px"
           
              alt="React"
            />
            </div>
            <div className="navbar_list">
            {user && user.firstname ? (
              <div className="navbar_left">
                <ul>
                  <li>
                    <NavLink to='/'
                    className='NavLink'
                    >
                  Home
                </NavLink>
                </li>
                <li>

                <NavLink  to="/appointment"
                className='NavLink'
                >
                  Appointment
                </NavLink>
              
                </li>
                  <li>

                <NavLink to="/doctor"
                className='NavLink'
                >
                  Doctor
                </NavLink>
                  </li>
                    </ul>

                <div className="nav_login">
                  <h4>Hi,{user.firstname}</h4>
              
                <NavLink to="/logout"
                
                className='NavLink'
                >Logout</NavLink>
                </div>
               
                 
                </div>


            ):(
              <>
              <div className="navbar_right">

              <h4>Hospital Management System</h4>
                 <ul>
                  <li>

                  <NavLink to="/register" 
                  className='NavLink'
                  >
                  Sign Up
                </NavLink>
                </li>
                <li>
                <NavLink to="/login"
                className='NavLink'
                >
                  Log In
                </NavLink>
                  </li>
                 </ul>
              </div>
              </>
            )}
            </div>

          </nav>

         </div>
      


     
    </>
  );
}

const mapStateToProps= (rootStore)=>({
  user:rootStore.user.user

})


const mapDispatchToProps = {};
export const TopNavbar = connect(
  mapStateToProps,
  mapDispatchToProps
)(TopNavbarComponent);
