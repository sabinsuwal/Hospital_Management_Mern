import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import { useNavigate } from 'react-router-dom'
import { http } from '../../../services/http.services'
import { ActionBtns } from '../../common/action-btns.component'

export function ViewAppointment() {
let navigate= useNavigate()

const columns=[
      {name:'Name',
      selector:row=>row?.user_id?.firstname
  },
  {name:'Gender',
  selector:row=>row?.user_id?.gender
},
  {name:'Problem',
  selector:row=>row?.problem
},
{name:'Doctor',
selector:row=>row?.doctorname
},
 
  {
      name:'Appointment-Date',
      selector:row=>row?.appointmentDate
  },
  {name:'Appointment-Time',
      selector:row=>row?.appointmentTime
  },
  {
    name: 'Actions',
    selector: row => <ActionBtns 
    
    editUrl='/admin/appointment/appoint'
   
    id={row._id}
    onDelete={deleteAppointment}></ActionBtns>
},
]



const deleteAppointment=(id)=>{
  http.deleteItem('/admin/appointment/'+id)
  .then((res)=>{
      
      if (res.data.status){
          if (isLoading){
              
              setIsLoading(false)
          }
          else{
              setIsLoading(true)
          }
      }else{
          console.log('err in deletering')
      }
  })
  .catch((err)=>{
      console.log('err',err)
  })
}




  
   
  
    
 

    let [appointment, setAppointment] = useState();
    let [isLoading, setIsLoading] =useState(true);
    
    useEffect(()=>{
        http.getItem('/admin/appointment')
        
        .then((res)=>{
            
            
            if (res.data.status){
                let all_result=[]
                
                all_result=res.data.data.map((o)=>o)
                setAppointment(all_result)
            }
        })
        .catch((err)=>{
            console.log('err in retirv',err)
        })
    },[])
    
    
  return (
    <>

     
         



      <h4 style={{ textAlign:'center', margin:'40px 0px', 
      fontWeight:'bold',
      fontSize:'40px'}}>
            Appointment List
          
        </h4>

        <hr />
        <DataTable
            columns={columns}
            data={appointment}
            pagination
            responsive
        />

 



    </>
  )
}


