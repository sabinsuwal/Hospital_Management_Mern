import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { http } from "../../../services/http.services";

export function EditAppointment() {
  let [user, setUser] = useState("");
  let [problem, setProblem] = useState("");
  let [age, setAge] = useState("");
  let [gender, setGender] = useState("");
  let [state, setState] = useState();
  let [department, setDepartment] = useState("Cardiologist");
  let [doctorname, setDoctorname] = useState('');
  let [doctornames, setDoctornames]=useState([])
  let [appointDate, setAppointDate] = useState("");
  let [appointTime, setAppointTime] = useState("");
  let navigate = useNavigate();

  let param = useParams();

  useEffect(() => {
    http
      .getItem("/admin/doctor/dep/" + department)
      .then((res) => {
        console.log(res.data.data);
        setDoctornames(res.data.data);
      })
      .catch((er) => {
        console.log(er);
      });
  }, [department]);
console.log(doctorname)

  useEffect(() => {
    http
      .getItem("/admin/appointment/appoint/" + param.id)
      .then((res) => {
        if (res.data.status) {
          setState(res.data.data);
          setProblem(res.data.data.problem);
          setAge(res.data.data.age);
          setGender(res.data.data.user_id.gender);
        } else {
          console.log(res.data.data.msg);
        }
      })
      .catch((er) => {
        console.log(er);
      });
  }, [user]);

  const onSubmit = (e) => {
    e.preventDefault();
    let data = {
      ...state,
      department: department,
      appointed: "yes",
      doctorname: doctorname,
      appointmentDate: appointDate,
      appointmentTime: appointTime,
    };
    console.log(data);
    http
      .updateItem("/admin/appointment/" + data._id, data)
      .then((res) => {
        console.log(res.data);
        if (res.data.status) {
          navigate("/admin/appointment");
        } else {
          console.log(res.data.msg);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
    <div className="register_container">
      <div className="register_wrapper">

     
      <form
       
        onSubmit={onSubmit}
      >
      
          
            <div className="form_control">
              <label> Problems</label>
              <input
                type="textbox"
                defaultValue={problem}
                disabled
                onChange={(e) => setProblem(e.target.value)}
                className="form-control"
                required
              ></input>
            </div>
            <div className="form_control">
              <label>AGE</label>
              <input
                type="text"
                defaultValue={age}
                disabled
                onChange={(e) => setAge(e.target.value)}
                className="form-control"
                required
              ></input>
            </div>
            <div className="form_control">
              <label>Gender</label>
              <input
                type="text"
                defaultValue={gender}
                disabled
                onChange={(e) => setGender(e.target.value)}
                className="form-control"
                required
              ></input>
            </div>
            <div className="form_control">
              <label style={{width:'35%'}}> Department</label>
              <select
                  style={{width:'40%'}}
               onChange={(e) => setDepartment(e.target.value)}
               >
                <option disabled> --Select Department--</option>
                <option selected={department=='Cardiologist'} value="Cardiologist">
                  Cardiologist
                </option>
                <option selected={department=='Dermatologists'} value="Dermatologists">
                  Dermatologists
                </option>
                <option selected={department=='Medicine Specialists'} value="Medicine Specialists">
                  Medicine Specialist
                </option>
                <option selected={department=='Allergists'} value="Allergists">
                  Allergists
                </option>
                <option selected={department=='Anesthesiologists'} value="Anesthesiologists">
                  Anesthesiologist
                </option>
                <option selected={department=='Colon Surgeons'} value="Colon Surgeons">
                  Colon Surgeons
                </option>
              </select>
            </div>
            <div className="form_control">
              <label style={{width:'35%'}}> Doctor</label>
              <select
                name="category"
                
                onChange={(e)=>setDoctorname(e.target.value)}
            
                id="category"
                style={{width:'40%'}}
              >
                <option disabled> --Select doctor--</option>
                {doctornames.map((o, i) => (
                    
                  <option
                    key={i}
                    value={o.firstname}
                  
                  >{o.firstname}
                  
                  </option>
                ))}
              </select>
            </div>
            <div className="form_control">
              <label>Appointment Date</label>
              <input
                type="Date"
                onChange={(e) => setAppointDate(e.target.value)}
                className="form-control"
                required
              ></input>
            </div>{" "}
            <div className="form_control">
              <label>AppointmentTime</label>
              <input
                type="time"
                placeholder="e.g 8am-8.20am"
                onChange={(e) => setAppointTime(e.target.value)}
                className="form-control"
                required
              ></input>
            </div>
            <div className="d-grid form_control">
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
         
      </form>
      </div>
    </div>
    </>
  );
}
