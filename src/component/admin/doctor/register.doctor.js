import React from "react";
import { http } from "../../../services/http.services";
import { FormDoctor } from "./rform.admin.doctor";
import { useNavigate } from "react-router-dom";

export function RegisterDoctor() {
  let navigate = useNavigate();

  const registerDoctor = (data) => {
    http
      .postItem("/admin/doctor/register", data)
      .then((res) => {
        if (res.data.status) {
          console.log(res.data.msg);
          navigate("/admin/doctor");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
  
  <h4 style={{ textAlign:'center', margin:'40px 0px', 
      fontWeight:'bold',
      fontSize:'30px'}}>
        Register Doctor
        </h4>
      
      <FormDoctor onsubmit={registerDoctor}></FormDoctor>
    </>
  );
}
