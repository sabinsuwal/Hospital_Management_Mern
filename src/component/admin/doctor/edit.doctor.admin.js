import React, { useEffect, useState } from "react";
import { FormDoctor } from "./rform.admin.doctor";
import { http } from "../../../services/http.services";
import { useNavigate, useParams } from "react-router-dom";

export function EditDoctor() {
  let navigate = useNavigate();
  let param = useParams();
  let [doctor, setDoctor] = useState({});
  const onSubmit = (data) => {
       http.updateItem('/admin/doctor/'+param.id, data)
       .then((res)=>{
           if(res.data.status){
               navigate('/admin/doctor')
           }
           else{
               console.log(res.data.msg)
           }

       })
       .catch((err)=>{
           console.log(err)
       })
  };

  useEffect(() => {
    http
      .getItem("/admin/doctor/" + param.id)
      .then((res) => {
          
        if (res.data.status) {
          setDoctor(res.data.data);
        } else {
            console.log("err", err);
            navigate("/admin/doctor");
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, []);
  return (
    <>
       <h4 style={{ textAlign:'center', margin:'40px 0px', 
      fontWeight:'bold',
      fontSize:'30px'}}>
        Edit  Doctor
        </h4>
      <FormDoctor doctor={doctor} onsubmit={onSubmit} />
    </>
  );
}
