import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom';
import { http } from '../../../services/http.services';
import DataTable from 'react-data-table-component'
import { ActionBtns } from '../../common/action-btns.component';
export default function DoctorList() {
    const columns=[
        {name:'FirstName',
        selector:row=>row.firstname
    },{
        name:'Address',
        selector:row=>row.address
    },
    {
        name:'Department',
        selector:row=>row.department
    },
    {name:'phone',
        selector:row=>row.phone
    },
    {
        name: 'Actions',
        selector: row => <ActionBtns editUrl="/admin/doctor"
        id={row._id}
        onDelete={deleteDoctor}></ActionBtns>
    },
]
    let [data, setData] = useState();
    let [isLoading, setIsLoading] =useState(true);
    
    useEffect(()=>{
        http.getItem('/admin/doctor')
        
        .then((res)=>{
            console.log(res.data)
            
            if (res.status){
                let all_result=[]
                all_result=res.data.data.map((o)=>o)
                setData(all_result)
            }
        })
        .catch((err)=>{
            console.log('err in retirv',err)
        })
    },[isLoading])

    const deleteDoctor=(id)=>{
        http.deleteItem('/admin/doctor/'+id)
        .then((res)=>{
            
            if (res.data.status){
                if (isLoading){
                    
                    setIsLoading(false)
                }
                else{
                    setIsLoading(true)
                }
            }else{
                console.log('err in deletering')
            }
        })
        .catch((err)=>{
            console.log('err',err)
        })
    }







  return (
    (<>
              <div className='admin_doctor_header'>

              <h4 style={{ textAlign:'center', margin:'40px 0px', 
      fontWeight:'bold',
      fontSize:'30px'}}>
            Doctor List
            </h4>
            <NavLink className="btn btn-sm btn-success float-right" to="/admin/doctor/register">
                Add doctor
            </NavLink>
        
              </div>

        <hr />
        <DataTable
            columns={columns}
            data={data}
            pagination
            responsive
        />
    </>)
  )
}
