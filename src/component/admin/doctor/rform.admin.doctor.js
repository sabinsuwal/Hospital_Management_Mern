import React, { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";

export function FormDoctor(props) {

  let [doctor ,setDoctor]= useState({})
  let [firstname, setFirstName] = useState("");
  let [lastname, setLastName] = useState("");
  let [gender, setGender] = useState("male");
  let [address, setAddress] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [department, setDepartment] = useState("Cardiologist");
  let [phone, setPhone] = useState("");
  let navigate = useNavigate();

  useEffect(()=>{
    if (props.doctor){

      setFirstName(props.doctor.firstname)
      setLastName(props.doctor.lastname)
      setGender(props.doctor.gender)
      setAddress(props.doctor.address)
      setEmail(props.doctor.email)
      setPassword(props.doctor.password)
      setDepartment(props.doctor.department)
      setPhone(props.doctor.phone)
      setDoctor(props.doctor)
    }
  },[props])
  const onSubmit = (e) => {
    e.preventDefault();
    let data_reg = {
      firstname: firstname,
      lastname: lastname,
      gender: gender,
      address: address,
      email: email,
      password: password,
      department: department,
      phone: phone,
    };
    props.onsubmit(data_reg)

   
  };

  return (
    <>
      
          <div className="register_container">
        <div className="register_wrapper">
      <form
      
        onSubmit={onSubmit}
      >
            <div className="form_control">
              <label>First Name</label>
              <input
                type="text"
                defaultValue={doctor?.firstname}
                name="firstname"
                onChange={(e) => setFirstName(e.target.value)}
                className="form-control"
                placeholder="firstName"
                required
              ></input>
            </div>
            <div className="form_control">
              <label>Last Name</label>
              <input
                type="text"
                defaultValue={doctor?.lastname}
                name="lastname"
                onChange={(e) => setLastName(e.target.value)}
                className="form-control"
                placeholder="lastName"
                required
              ></input>
            </div>

            <div className="form_control">
              <label style={{width:'40%'}}>Select Gender</label>
              <select
              style={{width:'53%'}}
              defaultValue={doctor?.gender} onChange={(e) => setGender(e.target.value)}>
             
                <option selected={(doctor.gender=='male')??'false'} value="male">Male</option>
                <option selected={(doctor.gender=='female')??'false'} value="female">Female</option>
                <option selected={(doctor.gender=='other')??'false'} value="other">Other</option>
             
             </select>
            </div>
            <div className="form_control">
              <label>Address</label>
              <input
              defaultValue={doctor?.address}
                type="text"
                className="form-control"
                placeholder="address"
                required
                onChange={(e) => setAddress(e.target.value)}
              ></input>
            </div>

            <div className="form_control">
              <label>Email</label>
              <input
                type={"email"}
                defaultValue={doctor?.email}
                className="form-control"
                placeholder="abc@gmail.com"
                required
                onChange={(e) => setEmail(e.target.value)}
              ></input>
            </div>
            <div className="form_control">
              <label>Password</label>
              {doctor && doctor.password?(
              <input
                type="password"
              
                className="form-control"
                placeholder="password"
                required
                disabled
               
              ></input>):(
                <input
                type="password"
                defaultValue={doctor?.password}
                className="form-control"
                placeholder="password"
                required
                onChange={(e) => setPassword(e.target.value)}
              ></input>
              )}
            </div>
            <div className="form_control">
              <label>Phone No</label>
              <input
                type='text'
                className="form-control"
                placeholder="valid phoneno"
                required
                defaultValue={doctor?.phone}
                onChange={(e) => setPhone(e.target.value)}
              ></input>
            </div>
            <div className="form_control">
              <label
                    style={{width:'40%'}}
              >Select Department</label>
              <select 
                    style={{width:'53%'}}
              defaultValue={doctor?.department} onChange={(e) => setDepartment(e.target.value)}>
                <option selected={(doctor.department=='Cardiologist')??'false'} value="Cardiologist">Cardiologist</option>
                <option selected={(doctor.department=='Dermatologists')??'false'}  value="Dermatologists">Dermatologists</option>
                <option selected={(doctor.department=='Medicine Specialists')??'false'}  value="Medicine Specialists">Medicine Specialist</option>
                <option selected={(doctor.department=='Allergists')??'false'}  value="Allergists">Allergists</option>
                <option selected={(doctor.department=='Anesthesiologists')??'false'}  value="Anesthesiologists">Anesthesiologist</option>
                <option selected={(doctor.department=='Colon Surgeons')??'false'}  value="Colon Surgeons">Colon Surgeons</option>
              </select>
            </div>
            <div className=" form_control">


              {(doctor?.firstname)?
            (
              <button type="submit" >
                Update
              </button>
            ):(

              <button type="submit" >
                Sign Up
              </button>
            )  
            }
              
            </div>
      </form>
          </div>
        </div>
    </>
  );
}
