


import React from "react";

import { NavLink } from "react-router-dom";

export function TopNavbar() {
 
  

  return (
    <>


<div className="nav_container">
          <nav className="main_navbar">
            <div className="navbar_img">
            <img
             src={process.env.PUBLIC_URL + "img/hospital.jpeg"}
              width="60px"
              height="30px"
           
              alt="React"
            />
            </div>
            <div className="navbar_list">
          
              <div className="navbar_left">
                <ul>
                  <li>
                    <NavLink to='/admin'
                    className='NavLink'
                    >
                  Home
                </NavLink>
                </li>
                <li>

                <NavLink className="NavLink" 
                to="/admin/appointment">
                  Appointment
                </NavLink>
              
                </li>
                  <li>

                <NavLink to="/admin/doctor"
                className='NavLink'
                >
                  Doctor
                </NavLink>
                  </li>
                    </ul>

                <div className="nav_login">
                 
              
                <NavLink to="/logout"
                style={{marginTop:'-8px'}}
                className='NavLink'
                >Logout</NavLink>
                </div>
               
                 
                </div>


           
            </div>

          </nav>

         </div>
   
    </>
  );
}
