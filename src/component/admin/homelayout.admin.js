import React, { Component } from 'react'
import { Outlet } from 'react-router-dom'
import { Footer } from '../footer/footer'
import { TopNavbar } from './topnavbar.admin'
export class AdminHomeLayout extends Component {
  render() {
    return (
     <>
      <TopNavbar />
        <Outlet>
          
          </Outlet>
          <Footer />
       
     </>
     
   
    )
  }
}



