import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import {  Pagination ,Navigation} from "swiper";

import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import "swiper/components/navigation/navigation.min.css";


import './style.css'

export class AdminHome extends React.Component {
  render() {
    return (
      <>
     <div className="home_container">
      <div className='wrapper'
 
      >
        <div className="swapper" id='swapper'>

           <h5> Making Health
            <br/>
            Care Better Together
           </h5>
           
        </div>

      </div>

     </div>
      </>
    );
  }
}
