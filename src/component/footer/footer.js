import React from "react";
import { NavLink } from "react-router-dom";
import "./footer.css";
export function Footer() {
  return (
    <>

  <div className="hosp_footer">
    <div className="wrapper">
      <h3> Brief Description </h3>
      <div className="footer_content">
        <div className="about_us">
            <h3>About Us</h3>
            <p>
              We are leading hospital in Nepal
            </p>
        </div>
        <div className="ourservice">
             <h3>Our Service</h3>
             <p>
              Excellent facilaties
             </p>
        </div>
        <div className="faq">
          <h3>FAQ</h3>
          <p>
            50% discout in Service
          </p>

        </div>
        <div className="contact">
            <h3>Contact Us</h3>
            <p>
              hosptial@gmail.com
            </p>
        </div>
      </div>

    </div>

    <div className="copyright">
    <p>
        Copyright © 2022. All Rights Reserved.</p>

  </div>

  </div>

     

    </>
  );
}
