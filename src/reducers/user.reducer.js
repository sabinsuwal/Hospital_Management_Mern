import { UserActionTypes } from "../action/useraction";


export const UserReducer=(state,action)=>{
    
    switch(action.type){
        case UserActionTypes.LOGIN:
            return {
                ...state,
                user:action.payload 
            }
        default:
            return {
                ...state
            }
    }
}