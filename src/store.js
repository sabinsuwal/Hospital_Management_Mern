import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import {RootReducer} from './reducers'

const middlewares=[thunk];

const defaultState={
    user:{
        user:{},
        isLoading:false
    }
}

export const store= createStore(RootReducer, defaultState, applyMiddleware(...middlewares))